/**
 * Created by hoangdieu on 10/19/15.
 */
(function ($) {
    $.extend($.fn,{
        checkForm : function (options) {
            console.log("start");
            var validatator = new $.checkValidator(options,$(this)[0]);
            $.data( this[ 0 ], "validator", validatator );

            if(!validatator.settings.html5Validate)
            {
                this.attr( "novalidate", "novalidate" );
            }

            this.find(":submit").attr( "onclick", '' );

            this.on( "submit.validate", function( event ) {
                if(!validatator.settings.isSubmited)
                {
                    validatator.settings.isSubmited = true;
                }
                if(!$.checkValidator.defaults.beforeConfirmFunc()){
                    return false;
                }
                if(!validatator.valid()){
                    validatator.settings.isSubmited = false;
                    return false;
                } else {
                    validatator.settings.isSubmited = false;
                    return $.checkValidator.defaults.confirmFunc(event);
                }
            });
            return validatator;
        },
        rules : function(){
            var elem = this,attr = {},rules = {};

            if(elem && elem.length){
                $.each(elem.get(0).attributes, function(v,n) {
                    n = n.nodeName||n.name;
                    v = elem.attr(n); // relay on $.fn.attr, it makes some filtering and checks
                    if(v != undefined && v !== false) attr[n] = v
                });
            }
            $.each(attr,function(i,val){
                if(i == "type" && val == "email"){
                    rules.email = true;
                } else if(i == "type" && val == "number"){
                    rules.number = true;
                } else if (i == "required"){
                    rules.required = true;
                } else if (i == "minlength"){
                    rules.minlength = parseInt(val);
                } else if (i == "maxlength"){
                    rules.maxlength = parseInt(val);;
                } else if (i == "pattern"){
                    rules.pattern = val;
                } else if (i == "character"){
                    switch (val){
                        case "hiragana" : rules.hiragana = true;break;
                        case "number" : rules.number = true;break;
                        case "digits" : rules.digits = true;break;
                        case "email" : rules.email = true;break;
                        default : break;
                    }
                } else {
                    rules[i] = val;
                }
            });
            if(rules.minlength > 0 && rules.maxlength > 0){
                rules.rangelength = [rules.minlength,rules.maxlength];
                delete rules.minlength;
                delete rules.maxlength;
            }
            return rules
        }
    });

    // constructor for validator
    $.checkValidator = function( options ,form) {
        this.settings = $.extend( true, {}, $.checkValidator.defaults, options );
        this.currentForm = form;
        this.init();
    };

    $.checkValidator.format = function( source, params ) {
        if(!params){
            return source;
        }
        if ( params.constructor !== Array ) {
            params = [ params ];
        }
        $.each( params, function( i, n ) {
            source = source.replace( new RegExp( "\\{" + i + "\\}", "g" ), function() {
                return n;
            });
        });
        return source;
    };

    $.extend( $.checkValidator, {
        defaults : {
            domNodeError: null,
            html5Validate : false,
            classError: 'icon-error',
            classNoError: 'icon-normal',
            borderError: 'border-red',
            classLabelError: 'label-error',
            showMessage: true,
            showPopup : false,
            errorMessage : '',
            parentNodeTagName : "tr",
            isSubmited : false,
            errorElement: "label",
            confirmFunc : function () {
                console.log("confirmFunction");
                return true;
            },
            beforeConfirmFunc : function () {
                console.log("beforeConfirmFunc");
                return true;
            },
            showPopupFunc : function(message){
                alert(message);
            },
            onfocusout: function( element ) {
                this.resetError(element);
                this.check(element);
            },
            onfocusin : function(element){

            },
            onchange : function(element){
                this.resetError(element);
                if(this.elementValue(element) != "" ){
                    this.check(element);
                }
            }
        },

        setDefaults: function( settings ) {
            $.extend($.checkValidator.defaults, settings );
        },

        messages: {
            required: "This field is required.",
            email: "Please enter a valid email address.",
            number: "Please enter a valid number.",
            hiragana : "Please enter hiragana characters.",
            maxlength: "Please enter no more than {0} characters.",
            digits: "Please enter only digits.",
            minlength: "Please enter at least {0} characters.",
            rangelength: "Please enter a value between {0} and {1} characters long.",
            pattern : "Please enter a valid text.",
            max: "Please enter a value less than or equal to {0}.",
            min: "Please enter a value greater than or equal to {0}."
        },
        prototype:{
            init : function(){
                function delegate( event ) {
                    var validator = $.data( this.form, "validator" ),
                        eventType = "on" + event.type.replace( /^validate/, "" ),
                        settings = validator.settings;
                    if ( settings[ eventType ] ) {
                        settings[ eventType ].call( validator, this, event );
                    }
                }

                $( this.currentForm )
                    .on( "focusin.validate focusout.validate change.validate",
                    ":text, [type='password'], [type='file'], select, textarea, [type='number']," +
                    "[type='email'], [type='date'], [type='radio'], [type='checkbox']", delegate)
                    .on("click.validate", "select, option, [type='radio'], [type='checkbox']", delegate);
            },

            checkable: function( element ) {
                return ( /radio|checkbox/i ).test( element.type );
            },

            findByName: function( name ) {
                return $( this.currentForm ).find( "[name='" + name + "']" );
            },

            elementValue: function( element ) {
                var val,
                    $element = $( element ),
                    type = element.type;

                if ( type === "radio" || type === "checkbox" ) {
                    return this.findByName( element.name ).filter(":checked").val();
                } else if ( type === "number" && typeof element.validity !== "undefined" ) {
                    return element.validity.badInput ? false : $element.val();
                }

                val = $element.val();
                if ( typeof val === "string" ) {
                    return val.replace(/\r/g, "" );
                }
                return val;
            },

            valid : function(){
                this.resetAllError();
                var valid = true,validator = this,first = true;
                $( this.currentForm )
                    .find( "input, select, textarea" )
                    .not( ":submit, :reset, :image, :disabled").each(function(index){
                        valid = !validator.check(this) ? false : valid;
                        if(!valid && first){
                            first = false;
                            if(validator.settings.showPopup){
                                validator.settings.showPopupFunc(validator.settings.errorMessage);
                            }
                        }
                    });
                return valid;
            },

            check :function (element){
                var rules = $(element).rules(),validator = this;
                var valid = true;
                if(rules.required){
                    if(!$.checkValidator.methods.required.call(validator,this.elementValue(element),element)){
                        validator.showErrors(element,$.checkValidator.messages.required);
                        valid = false;
                        return valid;
                    }
                    if(rules.digits){
                        if(!$.checkValidator.methods.digits.call(validator,this.elementValue(element),element)){
                            validator.showErrors(element,$.checkValidator.messages.digits);
                            valid = false;
                            return valid;
                        }
                    }
                    if(rules.number){
                        if(!$.checkValidator.methods.number.call(validator,this.elementValue(element),element)){
                            validator.showErrors(element,$.checkValidator.messages.number);
                            valid = false;
                            return valid;
                        }
                    }
                    $.each(rules, function (name,val) {
                        if(name != 'digits' && name != "number") {
                            if ($.checkValidator.methods[name] && !$.checkValidator.methods[name].call(validator, validator.elementValue(element), element, val)) {
                                validator.showErrors(element, $.checkValidator.messages[name], val);
                                valid = false;
                                return false;
                            }
                        }
                    });
                } else if(element.type == 'text'){
                    if ($(element).val() != "") {
                        if(rules.digits){
                            if(!$.checkValidator.methods.digits.call(validator,this.elementValue(element),element)){
                                validator.showErrors(element,$.checkValidator.messages.digits);
                                valid = false;
                                return valid;
                            }
                        }
                        if(rules.number){
                            if(!$.checkValidator.methods.number.call(validator,this.elementValue(element),element)){
                                validator.showErrors(element,$.checkValidator.messages.number);
                                valid = false;
                                return valid;
                            }
                        }
                        $.each(rules, function (name,val) {
                            if(name != 'digits' && name != "number") {
                                if ($.checkValidator.methods[name] && !$.checkValidator.methods[name].call(validator, validator.elementValue(element), element, val)) {
                                    validator.showErrors(element, $.checkValidator.messages[name], val);
                                    valid = false;
                                    return false;
                                }
                            }
                        });
                    }
                }
                return valid;
            },

            getLength: function( value, element ) {
                switch ( element.nodeName.toLowerCase() ) {
                    case "select":
                        return $( "option:selected", element ).length;
                    case "input":
                        if ( this.checkable( element ) ) {
                            return this.findByName( element.name ).filter( ":checked" ).length;
                        }
                }
                return value.length;
            },

            isRequired : function(element){
                return $(element).attr("required") == "required" || $(element).hasClass("required");
            },

            showErrors: function(element,message,param){
                if($(element).closest(this.settings.parentNodeTagName).find("."+this.settings.classNoError).length > 0)
                {
                    $(element).closest(this.settings.parentNodeTagName).find("."+this.settings.classNoError)
                        .removeClass(this.settings.classNoError).addClass(this.settings.classError);
                }
                $(element).addClass(this.settings.borderError);
                if(this.settings.showMessage){
                    if($(element).parent().find("label."+this.settings.classLabelError).length == 0)
                    {
                        $(element).parent().append("<label class='"+this.settings.classLabelError+"'>"+ $.checkValidator.format(message,param)+"</label>");
                    } else {
                        $(element).parent().find("label."+this.settings.classLabelError).text( $.checkValidator.format(message,param));
                    }
                }
                this.settings.errorMessage = message;
            },
            resetError : function(element){

                $(element).removeClass(this.settings.borderError);
                if($(element).parent().find( "input, select, textarea").length <= 1)
                {
                    $(element).parent().find("label."+this.settings.classLabelError).remove();
                    $(element).closest(this.settings.parentNodeTagName).find("."+this.settings.classError)
                        .removeClass(this.settings.classError).addClass(this.settings.classNoError);
                } else {
                    var invalid = false,validator = this;
                    $(element).parent().find( "input, select, textarea")
                        .not("[name='"+$(element).attr("name")+"']").each(function(index){
                            if($(this).hasClass(validator.settings.borderError)) {
                                invalid = !validator.check(this);
                                if (invalid) return false;
                            }
                        });
                    if(!invalid){
                        $(element).parent().find("label."+this.settings.classLabelError).remove();
                        $(element).closest(this.settings.parentNodeTagName).find("."+this.settings.classError)
                            .removeClass(this.settings.classError).addClass(this.settings.classNoError);
                    }
                }
            },
            resetAllError : function(){
                $(this.currentForm).find("input,select,textarea").removeClass(this.settings.borderError);
                $(this.currentForm).find("."+this.settings.classError).removeClass(this.settings.classError).addClass(this.settings.classNoError);
                $(this.currentForm).find("label."+this.settings.classLabelError).remove();
                return false;
            }
        },

        addMethod: function( name, method, message ) {
            $.validator.methods[ name ] = method;
            $.validator.messages[ name ] = message !== undefined ? message : $.validator.messages[ name ];
        },

        methods:{
            required: function( value, element ) {
                if ( element.nodeName.toLowerCase() === "select" ) {
                    // could be an array for select-multiple or a string, both are fine this way
                    var val = $( element ).val();
                    return val && val.length > 0;
                }
                if ( this.checkable( element ) ) {
                    return this.getLength( value, element ) > 0;
                }
                return value.length > 0;
            },
            hiragana: function (value,element) {
                if($(element).attr("character") != "hiragana")
                    return true;
                // 全角ひらがな以外があったら不可
                for (i = 0; i < value.length; i++) {
                    var word = value.substr(i, 1);
                    if (word.match(/[^ぁ-んー]+/)) {
                        return false;
                    }
                }
                return true;
            },
            email: function( value, element ) {
                return /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test( value );
            },

            number: function( value, element ) {
                return /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test( value );
            },

            digits: function( value, element ) {
                return /^\d+$/.test( value );
            },

            minlength: function( value, element, param ) {
                var length = $.isArray( value ) ? value.length : this.getLength( value, element );
                if(this.isRequired(element)){
                    return length >= param;
                }
                if(length > 0){
                    return length >= param;
                }
                return true;
            },
            maxlength: function( value, element, param ) {
                var length = $.isArray( value ) ? value.length : this.getLength( value, element );
                if(this.isRequired(element)){
                    return length <= param;
                }
                if(length > 0){
                    return length <= param;
                }
                return true;
            },
            rangelength: function( value, element, param ) {
                var length = $.isArray( value ) ? value.length : this.getLength( value, element );
                if(this.isRequired(element)){
                    return length >= param[ 0 ] && length <= param[ 1 ];
                }
                if(length > 0){
                    return length >= param[ 0 ] && length <= param[ 1 ];
                }
                return true;
            },
            pattern : function(value,element,param){
                try {
                    var thisRegex = new RegExp(param, 'g');
                    return value.match(thisRegex) != null;
                }
                catch(e) {
                    return false;
                }
            },
            min: function( value, element, param ) {
                var length = $.isArray( value ) ? value.length : this.getLength( value, element );
                var intVal = parseInt(value);
                if(this.isRequired(element)){
                    return intVal >= parseInt(param);
                }
                if(length > 0){
                    return intVal >= parseInt(param);
                }
                return true;
            },
            max: function( value, element, param ) {
                var length = $.isArray( value ) ? value.length : this.getLength( value, element );
                var intVal = parseInt(value);
                if(this.isRequired(element)){
                    return intVal <= parseInt(param);
                }
                if(length > 0){
                    return intVal <= parseInt(param);
                }
                return true;
            }
        }
    });
}(jQuery));