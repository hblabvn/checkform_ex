## Config
```
$("form").checkForm();
```
## Setup validate form
When create a form, setting validate type for field (FormType in Form folder)
```
required
type='email'
character: numer,hiragana,digits
minlength
maxlength
pattern
```
## Customer message 

if you want to change message of validate you can use follow code:
```
$.extend($.checkValidator.messages, {
    required: "",
    email: "",
    number: "",
    hiragana: "",
    maxlength: "",
    minlength: "",
    rangelength: "",
    pattern: ""
});

```

## Disable show message
If you dont want to show message behind of input, you can setup :
```
$.checkValidator.setDefaults({showMessage : false});
```

## Show popup
If you want to show popup when errors and customer function show popup,you can use this code:
```
$.checkValidator.setDefaults({
   showPopup : false,
   showPopupFunc : function(message){
     dialog_alert(title,message);
   }
});
```
## Setup function confirm submit

```
$.checkValidator.setDefaults({
   confirmFunc : function(message){
     dialog_alert(title,message);
   }
});
```
## Add a validate function
```
$.checkValidator.addMethod(name,method,message);
`name` : validate name
`method`: process function with this validate
`message`: message that will showed when error
```
## Default setting of checkForm
```
defaults : {
  domNodeError: null,
  html5Validate : false,
  classError: 'icon-error',
  classNoError: 'icon-normal',
  borderError: 'border-error',
  classLabelError: 'label-error',
  showMessage: true,
  showPopup : false,
  errorMessage : '',
  parentNodeTagName : "tr",
  isSubmited : false,
  errorElement: "label",
  showPopupFunc : function(message){
      alert(message);
  },
  onfocusout: function( element ) {
      this.check(element);

  },
  onfocusin : function(element){

  },
  onkeyup : function(element){
      if(this.elementValue(element) !== ""){
          this.resetError(element);
      }
  }
```